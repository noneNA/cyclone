# Cyclone

Cyclone is a mod for [Risk of Rain Modloader](https://rainfusion.ml) that aims to make it easier to use, mod, and explore the game.  
The latest version can be found on the repository (https://gitlab.com/noneNA/cyclone) under the `develop` branch.  

## Includes

### Terminal

Press F12 to access an the in-game terminal.  
While the terminal is active you can press F12 to close it or Shift+F12 to move the focus back to the game.  
Note that some keys cannot be written normally (due to the way Modloader works) and can be accessed through F1-F11 and their shift-modified versions.  
The list of these keys can be found in `utilities/textinput/keys.lua`.  

### Commands

For a complete list of commands, use the command `list`.  
The `help` command also shows some basic commands.  
Commands currently don't have documentation but if you have modded before you should be able to read their code very easily.  

### Cursor

You can right-click to position the Cyclone cursor.  
Pressing middle-click will select the instance on the regular cursor.  
Holding and dragging middle-click will select multiple instances.  
The selection is used by some commands.  

### CycloneLib

A submod that holds a wide variety of utility functions not found within Modloader.  
To use it simply copy the folder `cyclonelib` under `libraries` into your mod's root folder and:  
```
require("cyclonelib/main")
```
More information can be found in its `README.md`  

## Contact

You can contact me using my mail adress `nonena@protonmail.com` or through the discord under the name `none`.  