-- Cyclone - main.lua

-- Dependencies:
---- Nothing

-- Scale Export (Thanks to Zarah for the insight)
function sc() export("Cyclone.sc", misc.getOption("video.scale")) end ; sc()
registercallback("preStep", sc)

local SUBMOD = false
local SUBMODROOT = "cyclone/"
local RESTREFILE = "libraries/restre"
if not RESTRE_DIR then
	if SUBMOD then
		RESTRE_DIR = SUBMODROOT
		require(RESTRE_DIR .. RESTREFILE)()
	else
		require(RESTREFILE)()
	end
end

if not CycloneLib then
	restre_require("libraries/cyclonelib/main")
end

local resources = restre_require("resources/resources")
export("Cyclone.resources", resources)

restre_require("contents/cursor/cursor")

restre_require("utilities/info")
restre_require("utilities/pause")
restre_require("utilities/textinput/textinput")
restre_require("utilities/nsh")
restre_require("utilities/superuser")
restre_require("utilities/toggler")

restre_require("contents/terminal/terminal")
restre_require("contents/terminal/terminal_gui")

restre_require("contents/commands/commands")
restre_require("contents/commands/aliases")