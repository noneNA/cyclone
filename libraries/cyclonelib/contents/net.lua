-- CycloneLib - contents/net.lua

-- Dependencies:
---- Nothing

local _net = {}

-- Returns a function, that when executed will execute the given function with the given arguments on all players
_net.AllPacket = function(name, f)
	local host = net.Packet.new(name .. "_host", function(sender, ...)
		f(...)
	end)
	local client = net.Packet.new(name .. "_client", function(sender, ...)
		f(...)
		host:sendAsHost(net.EXCLUDE, sender, ...)
	end)
	return function(...)
		f(...)
		host:sendAsHost(net.ALL, nil, ...)
		client:sendAsClient(...)
	end
end

--#########--
-- Exports --
--#########--

export("CycloneLib.net", _net)
return _net