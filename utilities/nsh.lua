-- Cyclone - utilities/nsh.lua

-- Dependencies:
---- CycloneLib.modloader
---- CycloneLib.table
---- CycloneLib.string
---- Cyclone.Terminal

local nsh = {}
local priority_namespace = nil

local function checkClass(class)
	if (not _G[class]) or (not _G[class].find) then
		error(string.format("Class '%s' does not exist", class))
	end
end

local function matchNamespace(object, namespace)
	return namespace and object:getOrigin() == namespace
end

local function matchExact(s, t)
	return s:lower() == t:lower()
end

local function matchSub(s, t)
	return string.find(s:lower(), t:lower()) ~= nil
end

local function matchLetter(s, t)
	return string.find(s:lower():gsub("%W",''), t:lower():gsub("%W",'')) ~= nil
end

local MATCH = {}
MATCH[1] = function(object, name)
	return matchExact(object:getName(), name)
end
MATCH[2] = function(object, name)
	return matchSub(object:getName(), name)
end
MATCH[3] = function(object, name)
	return matchLetter(object:getName(), name)
end
MATCH[4] = function(object, name)
	return CycloneLib.string.levenshtein(object:getName():lower(), name:lower()) <= 4
end

nsh.getMaxMatch = function()
	return #MATCH
end

nsh.all = function(class)
	checkClass(class)
	
	local objects = {}
	
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,object in ipairs(_G[class].findAll(namespace)) do
			table.insert(objects, object)
		end
	end
	
	return objects
end

-- Force values higher than nsh.getMaxMatch() will always return an object (as long as an object exists)
nsh.find = function(name, class, force)
	checkClass(class)
	
	-- The last non-fuzzy match is currently 3
	local force = force or 3
	
	if name == nil then
		if force > #MATCH then
			name = ""
		else
			return nil
		end
	end
	
	local found_object
	local found_priority
	local found_namespace
	
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,object in ipairs(_G[class].findAll(namespace)) do
			local least_priority = found_object and (found_namespace and found_priority - 1 or found_priority) or #MATCH
			least_priority = math.min(force, least_priority)
			local match_priority
			for i=1,least_priority do
				if MATCH[i](object, name) then
					match_priority = i
					break
				end
			end
			if not match_priority then
				match_priority = least_priority + 1
			end
			
			if
				(not found_priority)
				or
				(match_priority < found_priority)
				or
				(priority_namespace and (not found_namespace) and matchNamespace(object, priority_namespace))
			then
				found_object = object
				found_priority = match_priority
				found_namespace = (not priority_namespace) or matchNamespace(object, priority_namespace)
			end
			
			if (found_priority == 1) and found_namespace then break end
		end
		if (found_priority == 1) and found_namespace then break end
	end
	
	if found_priority <= force then return found_object end
end

nsh.error = function(name, class)
	return string.format("Couldn't find the %s with name '%s'", class, name)
end

nsh.findAll = function(name, class)
	checkClass(class)
	
	local name = name or ""
	local found_objects = {}
	
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,object in ipairs(_G[class].findAll(namespace)) do
			if matchLetter(object:getName(), name) then
				table.insert(found_objects, object)
			end
		end
	end
	
	return found_objects
end

nsh.suggestions = function(name, class, seperator)
	local object_names = {}
	for _,object in ipairs(nsh.findAll(name, class)) do
		table.insert(object_names, string.format("%s [%s]", object:getName(), object:getOrigin()))
	end
	local suggestions = CycloneLib.table.listString(object_names, seperator or "\n")
	return suggestions ~= "nil" and suggestions or "No suggestions"
end

nsh.findNamespace = function(name)
	local name = name or ""
	local found_namespace
	
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		if matchLetter(namespace, name) then
			found_namespace = namespace
		end
	end
	
	return found_namespace
end

nsh.setNamespace = function(namespace, noerror)
	if not CycloneLib.modloader.checkNamespace(namespace) then
		if noerror then
			return false
		else
			error(string.format("nsh: Namespace '%s' not found", namespace))
		end
	end
	priority_namespace = namespace
	return true
end

nsh.unsetNamespace = function()
	priority_namespace = nil
end

nsh.getNamespace = function()
	return priority_namespace
end

nsh.newCommand = function(name, class, other_function)
	checkClass(class)
	Cyclone.Terminal.addCommand(name, function(args, ...)
		if args[1] == "find" or args[1] == "search" then
			Cyclone.print(nsh.suggestions(args[2], class, ", "))
		elseif args[1] == "list" or args[1] == "all" then
			Cyclone.print(nsh.suggestions("", class, ", "))
		else
			other_function(args, ...)
		end
	end)
end


--#########--
-- Exports --
--#########--

export("Cyclone.nsh", nsh)
return nsh