-- Cyclone - utilities/textinput/textinput.lua

-- Dependencies:
---- Nothing

--[[
For gathering text input.
--]]

restre_require("keys")

local TextInput = {}

local REPEAT_FREQUENCY = 4
local REPEAT_START = 12
local function keyRepeat(duration)
	if duration == nil then return false end
	return (duration == 1) or ((duration > REPEAT_START) and ((duration - REPEAT_START) % math.ceil(REPEAT_FREQUENCY) == 0))
end

function TextInput.new()

	-- Local is necessary here, otherwise it will write everything to the last input field
	local field = { value = "" , active = false , cursor = 0 , input = {} }
	
	registercallback("onStep", function()
		if field.active then
			for k,v in pairs(Cyclone.Keys) do
				if input.checkKeyboard(v) ~= input.NEUTRAL then
					field.input[v] = (field.input[v] or 0) + 1
				else
					field.input[v] = nil
				end
			end
			
			local SHF  = input.checkKeyboard("shift") ~= input.NEUTRAL
			local CTRL = input.checkKeyboard("control") ~= input.NEUTRAL
			
			for k,v in pairs(Cyclone.Keys) do
				if (not CTRL) and keyRepeat(field.input[v]) then
					if Cyclone.TextKeys[k] then
						local character = ""
						if not SHF then character = Cyclone.TextKeys[k]
						else character = (Cyclone.ShiftKeys[k] or string.upper(Cyclone.TextKeys[k]))
						end
						local value = field.value
						field.value = value:sub(0, field.cursor) .. character .. value:sub(field.cursor + 1, -1)
						field.cursor = field.cursor + #character
					elseif v == "backspace" then field.value = field.value:sub(0, math.max(field.cursor - 1, 0)) .. field.value:sub(field.cursor + 1, -1) ; field.cursor = math.max(field.cursor - 1, 0)
					elseif v == "delete"    then field.value  = field.value:sub(0,field.cursor) .. field.value:sub(field.cursor+2,-1)
					elseif v == "left"      then field.cursor = math.max(field.cursor - 1, 0)
					elseif v == "right"     then field.cursor = math.min(field.cursor + 1, #field.value)
					elseif v == "end"       then field.cursor = #field.value
					elseif v == "home"      then field.cursor = 0
					end
				end
			end
		end
	end)
	
	-- Returns the contents of the field if there was a newline
	-- Does not contain newlines
	function field:pop()
		local index = field.value:find("\n")
		if not index then return nil end
		local line = field.value
		field.value = ""
		field.cursor = 0
		return line:gsub("\n", "")
	end
	
	-- Returns the contents of the last line if it exists and removes it
	-- Returns nil if a complete line doesn't exist
	function field:popLine()
		local index = field.value:find("\n")
		if not index then return nil end
		local line = field.value:sub(1, index)
		field.value = field.value:sub(index + 1, -1)
		field.cursor = field.cursor - #line
		-- Removes the \n at the end
		return line:sub(1,-2)
	end
	
	function field:reset() field.value = "" end
	function field:activate() field.active = true end
	function field:deactivate() field.active = false end
	function field:toggle() field.active = not field.active end
	
	return field
end


--#########--
-- Exports --
--#########--

export("Cyclone.TextInput", TextInput)
return TextInput