-- Cyclone - utilities/toggler.lua

-- Dependencies:
---- Cyclone.Terminal
---- CycloneLib.table
---- Cyclone.superuser

local ENABLE = {
"enable",
"on",
"start",
"resume",
"true",
}
ENABLE = CycloneLib.table.swap(ENABLE)

local DISABLE = {
"disable",
"off",
"stop",
"pause",
"false",
}
DISABLE = CycloneLib.table.swap(DISABLE)

local MESSAGE = {
[true] = "enabled",
[false] = "disabled",
}

local toggler = {}

toggler.new = function(command, name, default, enable, disable, superuser, sync)
	local store = {}
	store.state = default
	callback.register("onGameEnd", function()
		store.state = default
	end)
	
	local store_sync
	if sync then
		store_sync = CycloneLib.net.AllPacket("store_" .. command, function(state)
			store.state = state
		end)
	end
	
	Cyclone.Terminal.addCommand(command, function(args)
		if args[1] == "get" then
			Cyclone.print(string.format("%s is %s", name, MESSAGE[store.state]))
			return nil
		elseif superuser and (not Cyclone.superuser()) then
			Cyclone.error(command .. ": You don't have superuser")
			return nil
		end		
		
		local new_state
		
		if ENABLE[args[1]] then new_state = true
		elseif DISABLE[args[1]] then new_state = false
		else new_state = not store.state end
		
		if (enable and disable) and (new_state ~= store.state) then
			if new_state then enable()
			else disable() end
		end
		
		if new_state ~= store.state then
			if sync then
				store_sync(new_state)
			else
				store.state = new_state
			end
		end
		
		Cyclone.print(string.format("%s %s", name, MESSAGE[new_state]))
	end)
	
	return store
end


--#########--
-- Exports --
--#########--

export("Cyclone.toggler", toggler)
return toggler