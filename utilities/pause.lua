-- Cyclone - utilities/pause.lua

-- Dependencies:
---- Nothing

local STATE_VARIABLES = {
	"activity",
	"activity_var1",
	"activity_var2",
	"activity_type",
	"pVspeed",
	"pHspeed",
	"free",
	"jump_count",
}

local STAGE_RESET = {
	"activity",
	"activity_var1",
	"activity_var2",
	"activity_type",
	"free",
	"jump_count",
}


--#######--
-- Pause --
--#######--

local state
local function resetPause()
	state = {
		pause = false,
		detach = false,
	}
end
resetPause()
callback.register("onGameEnd", resetPause)

local pause
pause = setmetatable({}, { __call = function() pause.pause() end })

pause.pause = function()
	for _,player in ipairs(misc.players) do
		pause.attachPlayer(player)
		pause.detachPlayer(player)
	end
	state.pause = true
end

pause.unpause = function()
	for _,player in ipairs(misc.players) do
		pause.attachPlayer(player)
	end
	state.pause = false
end

pause.togglePause = function()
	if state.pause then
		pause.unpause()
	else
		pause.pause()
	end
end


--########--
-- Detach --
--########--

pause.detachPlayer = function(player)
	local d_player = player:getData()
	if not d_player.detached then
		d_player.pause_state = {}
		local state = d_player.pause_state
		
		for _,variable in ipairs(STATE_VARIABLES) do
			state[variable] = player:get(variable)
		end
		
		state.spriteSpeed = player.spriteSpeed
		state.subimage = player.subimage
		
		d_player.detached = true
	end
end

pause.attachPlayer = function(player)
	local d_player = player:getData()
	if d_player.detached then
		local state = d_player.pause_state
		if state then
			for _,variable in ipairs(STATE_VARIABLES) do
				if state[variable] then
					player:set(variable, state[variable])
				elseif variable == "activity" then
					player:set("activity", 0)
					player:set("activity_type", 0)
				end
			end
			
			player.spriteSpeed = state.spriteSpeed
			player.subimage = state.subimage
			
			d_player.pause_state = nil
			d_player.detached = false
		else
			print("ERROR: No player state found for player", player)
		end
	end
end

pause.toggleDetachPlayer = function(player)
	if player:getData().detached then
		pause.attachPlayer(player)
	else
		pause.detachPlayer(player)
	end
end

local detach_host = net.Packet.new("detach_host", function(sender, net_player)
	local player = net_player:resolve()
	if player then
		pause.detachPlayer(player)
	end
end)

local detach_client = net.Packet.new("detach_client", function(sender, net_player)
	local player = net_player:resolve()
	if player then
		pause.detachPlayer(player)
		detach_host:sendAsHost(net.EXCLUDE, player, player:getNetIdentity())
	end
end)

local attach_host = net.Packet.new("attach_host", function(sender, net_player)
	local player = net_player:resolve()
	if player then
		pause.attachPlayer(player)
	end
end)

local attach_client = net.Packet.new("attach_client", function(sender, net_player)
	local player = net_player:resolve()
	if player then
		pause.attachPlayer(player)
		attach_host:sendAsHost(net.EXCLUDE, player, player:getNetIdentity())
	end
end)

pause.detach = function()
	if not net.online then
		pause.pause()
		for i,player in ipairs(misc.players) do
			pause.detachPlayer(player)
		end
	else
		local player = net.localPlayer
		pause.detachPlayer(player)
		detach_client:sendAsClient(player:getNetIdentity())
		detach_host:sendAsHost(net.ALL, nil, player:getNetIdentity())
	end
	state.detach = true
end

pause.attach = function()
	if not net.online then
		pause.unpause()
		for i,player in ipairs(misc.players) do
			pause.attachPlayer(player)
		end
	else
		local player = net.localPlayer
		pause.attachPlayer(player)
		attach_client:sendAsClient(player:getNetIdentity())
		attach_host:sendAsHost(net.ALL, nil, player:getNetIdentity())
	end
	state.detach = false
end

pause.toggleDetach = function()
	if state.detach then
		pause.attach()
	else
		pause.detach()
	end
end


--######--
-- Step --
--######--

local function detachStep()
	for _,player in ipairs(misc.players) do
		local d_player = player:getData()
		if d_player.detached then
			local pause_state = d_player.pause_state
			
			-- Prevents attacks, skills, use item
			for alarm=0,5 do
				if alarm ~= 1 then
					player:setAlarm(alarm, player:getAlarm(alarm) + 1)
				end
			end
			
			player:set("pVspeed", 0)
			player:set("pHspeed", 0)
			player:set("moveLeft", 0)
			player:set("moveRight", 0)
			player:set("free", 1)
			player:set("activity_type", 2)
			
			-- Prevents feather
			player:set("jump_count", player:get("feather"))
			
			player.spriteSpeed = 0
			
			if pause_state then
				player.subimage = pause_state.subimage
			end
		end
	end
end

-- preStep so that time is properly stopped
callback.register("preStep", function()
	if state.pause then
		-- TimeStop
		misc.setTimeStop(misc.getTimeStop() + 1)
		
		-- Director Spawns
		misc.director:setAlarm(1, misc.director:getAlarm(1) + 1)	
	end
end)

callback.register("onStep", function()
	-- Detached Players
	detachStep()
end)

callback.register("onStageEntry", function()
	for _,player in ipairs(misc.players) do
		local d_player = player:getData()
		if d_player.detached then
			for _,variable in ipairs(STAGE_RESET) do
				d_player.pause_state[variable] = nil
			end
		end
	end
end)


--#########--
-- Exports --
--#########--

export("Cyclone.pause", pause)
export("Cyclone.unpause", pause.unpause)
export("Cyclone.togglePause", pause.togglePause)
return pause