-- Cyclone - utilities/info.lua

-- Dependencies:
---- CycloneLib.player

local info_functions = {
	{
		"GMObjectBase",
		function(object)
			return string.format("%s [%s]",
				object:getName(),
				object:getOrigin()
			)
		end
	},
	{
		"PlayerInstance",
		function(player)
			local i = CycloneLib.player.index(player)
			return string.format("%s [%i]:%i at (%i,%i) as %s",
				net.online and string.format("%q", player:get("user_name")) or "Player",
				i,
				player.id,
				player.x,
				player.y,
				player:getSurvivor():getName()
			)
		end
	},
	{
		"Instance",
		function(instance)
			return string.format("Instance %i:%s at (%i,%i)%s",
				instance.id,
				instance:getObject():getName(),
				instance.x,
				instance.y,
				instance.visible and "" or " [invisible]"
			)
		end
	},
}

local info
info = setmetatable({}, { __call = function(self, ...) return info.get(...) end })

info.get = function(thing)
	print(thing)
	for _,info_method in ipairs(info_functions) do
		if isa(thing, info_method[1]) then
			return info_method[2](thing)
		end
	end
	
	return tostring(thing)
end

--#########--
-- Exports --
--#########--

export("Cyclone.info", info)
return info