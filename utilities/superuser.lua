-- Cyclone - utilities/superuser.lua

-- Dependencies:
---- Nothing

local su
su = setmetatable({}, { __call = function() return su.check() end })

--[[
local su_host = net.Packet.new("su_host", function(sender, net_player, state)
	local player = net_player:resolve()
	if player then
		player:getData().superuser = state
	end
end)

local su_client = net.Packet.new("su_client", function(sender, net_player, state)
	local player = net_player:resolve()
	if player then
		player:getData().superuser = state
		su_host:sendAsHost(net.EXCEPT, sender, player:getNetIdentity())
	end
end)
--]]

local su_sync = CycloneLib.net.AllPacket("su", function(net_player, state)
	local player = net_player:resolve()
	if player then
		player:getData().superuser = state
	end
end)

su.check = function()
	local player = net.localPlayer or misc.players[1]
	return net.host or player:getData().superuser
end

su.grant = function(player)
	if su.check() then
		su_sync(player:getNetIdentity(), true)
		--player:getData().superuser = true
		--su_host:sendAsHost(net.ALL, nil, player:getNetIdentity(), true)
		--su_client:sendAsClient(player:getNetIdentity(), true)
	end
end

su.deny = function(player)
	if su.check() then
		su_sync(player:getNetIdentity(), false)
		--player:getData().superuser = false
		--su_host:sendAsHost(net.ALL, nil, player:getNetIdentity(), false)
		--su_client:sendAsClient(player:getNetIdentity(), false)
	end
end

su.get = function(player)
	return player:getData().superuser
end

--#########--
-- Exports --
--#########--

export("Cyclone.superuser", su)