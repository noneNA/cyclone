-- Cyclone - resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.fonts = {}

resources.fonts.dejavu = restre_fontFromFile("fonts/dejavu/DejavusansMono.ttf", 12)
resources.fonts.basis = restre_fontFromFile("fonts/basis33/basis33.ttf", 12)

return resources