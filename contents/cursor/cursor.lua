-- Cyclone - contents/cursor.lua

-- Dependencies:
---- CycloneLib.Rectangle
---- CycloneLib.modloader

local resources = restre_require("resources/resources")

local FADE_TIME = 4 * 60
local MIN_SELECT_SIZE = 16

local OBJECTS = {
	ParentObject.find("bosses"),
	ParentObject.find("enemies"),
	ParentObject.find("actors"),
	ParentObject.find("chests"),
	ParentObject.find("items"),
	ParentObject.find("droneItems"),
	ParentObject.find("mapObjects"),
	ParentObject.find("artifacts"),
	ParentObject.find("commandCrates"),
}
callback.register("postLoad", function()
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,object in ipairs(Object.findAll(namespace)) do
			table.insert(OBJECTS, object)
		end
	end
end)

local cursor = {
	x = 0,
	y = 0,
	prev_x = 0,
	prev_y = 0,
	fade = 0,
	box = nil,
	_selection = {},
	selected = {},
}
setmetatable(cursor, {
	__index = function(self, key)
		if key == "selection" then
			local selection = {}
			for key,instance in ipairs(cursor._selection) do
				if instance:isValid() then
					table.insert(selection, instance)
				else
					table.remove(cursor._selection, key)
				end
			end
			return selection
		else
			return rawget(self, key)
		end
	end
})

-- This doesn't show the cursor while moving it
local function playerCursor()
	local player = misc.players[1]
	if not player then return nil end
	cursor.x, cursor.y = player.x, player.y
	cursor.prev_x, cursor.prev_y = cursor.x, cursor.y
end
callback.register("onStageEntry", playerCursor)

local function resetCursor()
	cursor.x = 0
	cursor.y = 0
	cursor.prev_x = 0
	cursor.prev_y = 0
	cursor.fade = 0
	cursor.box = nil
	cursor._selection = {}
	cursor.selected = {}
	
	playerCursor()
end
callback.register("onGameStart", resetCursor)

--[[
local function validateSelection()
	for i,instance in ipairs(cursor._selection) do
		if not instance:isValid() then
			table.remove(cursor._selection, i)
		end
	end
end
callback.register("preStep", validateSelection)
callback.register("onStep", validateSelection)
callback.register("postStep", validateSelection)
--]]

local function selectOne(box)
	for _,object in ipairs(OBJECTS) do
		local instance = object:findRectangle(box.left, box.top, box.right, box.bottom)
		if instance and (not cursor.selected[instance]) then
			table.insert(cursor._selection, instance)
			cursor.selected[instance] = true
			break
		end
	end
end

local function selectAll(box)
	for _,object in ipairs(OBJECTS) do
		for _,instance in ipairs(object:findAllRectangle(box.left, box.top, box.right, box.bottom)) do
			local r_instance = CycloneLib.Rectangle.new(instance)
			if (not cursor.selected[instance]) and box:containsRectangle(r_instance) then
				table.insert(cursor._selection, instance)
				cursor.selected[instance] = true
			end
		end
	end	
end

callback.register("onStep", function()
	local x, y = input.getMousePos()
	
	cursor.fade = cursor.fade - 1
	if input.checkMouse("right") == input.PRESSED then
		cursor.x, cursor.y = x, y
	end
	if (cursor.prev_x ~= cursor.x) or (cursor.prev_y ~= cursor.y) then
		cursor.fade = FADE_TIME
	end
	
	cursor.prev_x, cursor.prev_y = cursor.x, cursor.y
	
	local middle = input.checkMouse("middle")
	if middle == input.PRESSED then
		cursor.box = CycloneLib.Rectangle.new(x,y,0,0)
	elseif middle == input.HELD then
		local box = cursor.box
		box.right = x
		box.bottom = y
	elseif middle == input.RELEASED then
		local box = cursor.box:correct()
		cursor.box = nil
		if input.checkKeyboard("control") == input.NEUTRAL then
			cursor._selection = {}
			cursor.selected = {}
		end
		if (box.w <= MIN_SELECT_SIZE) and (box.h <= MIN_SELECT_SIZE) then
			selectOne(box)
		else
			selectAll(box)
		end
	end
end)

callback.register("onDraw", function()
	graphics.drawImage{
		image = resources.sprites.cursor,
		x = cursor.x,
		y = cursor.y,
		alpha = (cursor.fade <= 0) and 0 or 1,
	}
	if cursor.box ~= nil then
		cursor.box:draw(Color.ROR_BLUE, 1/2)
	end
	for _,instance in ipairs(cursor._selection) do
		if instance:isValid() and instance.visible then
			local r_instance = CycloneLib.Rectangle.new()
			r_instance:fromInstance(instance, true)
			r_instance:draw(Color.ROR_BLUE, 1/4)
			graphics.color(Color.ROR_BLUE)
			graphics.rectangle(r_instance.left, r_instance.top, r_instance.right - 1, r_instance.bottom - 1, true)
			--[[
			graphics.drawImage{
				image = instance.sprite or instance.mask,
				x = instance.x,
				y = instance.y,
				subimage = instance.subimage,
				color = Color.ROR_BLUE,
				alpha = 1,
				angle = instance.angle,
				xscale = instance.xscale,
				yscale = instance.yscale,
			}
			--]]
		end
	end
end)


--#########--
-- Exports --
--#########--

export("Cyclone.cursor", cursor)
export("Cyclone.resources.cursor", resources)