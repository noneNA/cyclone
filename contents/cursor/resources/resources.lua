-- Cyclone - contents/cursor/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.cursor = restre_spriteLoad("cursor", "cursor.png", 1, 8, 8)

return resources