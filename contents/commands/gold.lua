-- Cyclone - contents/commands/gold.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.superuser
---- CycloneLib.net

local gold_sync = CycloneLib.net.AllPacket("gold", function(amount)
	misc.setGold(amount)
end)

Cyclone.Terminal.addCommand("gold", function(args)
	if args[1] == "get" then
		Cyclone.print(misc.getGold())
	else
		if not Cyclone.superuser() then
			Cyclone.error("gold: You don't have superuser")
			return nil
		end
		
		if args[1] == "set" then
			gold_sync(tonumber(args[2]) or misc.getGold())
		elseif args[1] == "give" or args[1] == "add" then
			gold_sync(misc.getGold() + (tonumber(args[2]) or 0))
		elseif args[1] == "take" or args[1] == "remove" then
			gold_sync(misc.getGold() - (tonumber(args[2]) or 0))
		else
			gold_sync(misc.getGold() + (tonumber(args[1]) or 0))
		end
	end
end)