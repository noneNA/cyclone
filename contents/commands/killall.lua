-- Cyclone - contents/commands/killall.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.superuser

local p_enemies = ParentObject.find("enemies")

local killall_sync = CycloneLib.net.AllPacket("killall", function()
	for _,i_enemy in ipairs(p_enemies:findAll()) do
		i_enemy:kill()
	end
end)

Cyclone.Terminal.addCommand("killall", function()
	if not Cyclone.superuser() then
		Cyclone.error("killall: You don't have superuser")
		return nil
	end
	
	killall_sync()
end)