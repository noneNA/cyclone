-- Cyclone - contents/commands/set.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor
---- CycloneLib.net
---- CycloneLib.table
---- Cyclone.superuser

local ALLOWED_TYPES = {
"nil",
"number",
"string",
}
ALLOWED_TYPES = CycloneLib.table.swap(ALLOWED_TYPES)

local set_sync = CycloneLib.net.AllPacket("set", function(net_instance, key, value)
	local instance = net_instance:resolve()
	if instance then
		instance:set(key, value)
	end
end)

Cyclone.Terminal.addCommand("set", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("set: You don't have superuser")
		return nil
	end
	
	local key = args[1] or ""
	
	local f, message = load(string.format("return %s", args[2]))
	local status, lua_value
	if not message then
		status, lua_value = pcall(f)
	end
	
	local value
	if args[2] == "nil" then
		value = nil
	elseif tonumber(args[2]) ~= nil then
		value = tonumber(args[2])
	elseif (not message) and status and (lua_value ~= nil) then
		value = lua_value
	else
		value = args[2]
	end
	
	local set = false
	for _,instance in ipairs(Cyclone.cursor.selection) do
		if not net.online then
			local status, message = pcall(instance.set, instance, key, value)
			if not status then
				Cyclone.error("set: Couldn't set '" .. key .. "'")
				Cyclone.error("set: Reason: " .. message)
				return nil
			else
				set = true
			end
		else
			if ALLOWED_TYPES[type(value)] then
				local status, message = pcall(instance.getNetIdentity, instance)
				if status then
					set_sync(instance:getNetIdentity(), key, value)
					set = true
				else
					Cyclone.error("set: Couldn't set '" .. key .. "'")
					Cyclone.error("set: Reason: " .. message)
				end
			end
		end
	end
	if not set then return nil end
	Cyclone.print(string.format("Set '%s' to '%s' (%s)", key, value, type(value)))
end)