-- Cyclone - contents/commands/timestop.lua

-- Dependencies
---- Cyclone.Terminal
---- CycloneLib.net
---- Cyclone.superuser

local function stop(duration, set)
	local duration = (duration or 0) * 60
	if set then
		misc.setTimeStop(duration)
		Cyclone.print(string.format("Set timestop to %s seconds", duration / 60))
	else
		misc.setTimeStop(misc.getTimeStop() + duration)
		Cyclone.print(string.format("Added %s seconds to timestop", duration / 60))
	end
end

local timestop_sync = CycloneLib.net.AllPacket("timestop", function(duration, set)
	stop(duration, set)
end)

Cyclone.Terminal.addCommand("timestop", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("timestop: You don't have superuser")
		return nil
	end
	
	if args[1] == "add" then
		timestop_sync(tonumber(args[2]))
	elseif args[1] == "set" then
		timestop_sync(tonumber(args[2]), true)
	else
		timestop_sync(tonumber(args[1]))
	end
end)