-- Cyclone - contents/commands/mods.lua

-- Dependencies
---- Cyclone.Terminal

local MODS_TEXT = [[
Current enabled mods (%s):
	%s
]]

Cyclone.Terminal.addCommand("mods", function()
	local mod_string = ""
	local mods = modloader.getMods()
	for _,mod_name in ipairs(mods) do
		mod_string = string.format(
			"%s%s%s [%s]",
			mod_string,
			mod_string == "" and "" or "\n\t",
			modloader.getModName(mod_name),
			mod_name
		)
	end
	Cyclone.print(string.format(MODS_TEXT, #mods, mod_string))
end)