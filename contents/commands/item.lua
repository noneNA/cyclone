-- Cyclone - contents/commands/item.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.nsh
---- Cyclone.cursor
---- CycloneLib.net
---- [Optional] IRL

local item_sync = CycloneLib.net.AllPacket("item_give", function(net_player, item, count)
	local player = net_player:resolve()
	if player then
		if item.isUseItem then player.useItem = item
		else player:giveItem(item, count) end
	end
end)

local remove_sync = CycloneLib.net.AllPacket("item_remove", function(net_player, item, count)
	local player = net_player:resolve()
	if player then
		if item.isUseItem then player.useItem = nil
		else player:removeItem(item, count) end
	end
end)

local function itemParse(name, count, player)
	local item = Cyclone.nsh.find(name, "Item")
	if not item then
		Cyclone.error("item: " .. Cyclone.nsh.error(name, "Item"))
	end
	
	local count = tonumber(count) or 1
	
	local player = misc.players[tonumber(player) or 1]
	if not player then
		Cyclone.error(string.format("item: Couldn't find the player with index '%s'", player))
		player = misc.players[1]
	end
	
	return item, count, player
end

local spawn_client = net.Packet.new("item_spawn_client", function(sender, object, x, y)
	object:create(x, y)
end)

Cyclone.nsh.newCommand("item", "Item", function(args)
	if not Cyclone.superuser() then Cyclone.error("item: You don't have superuser") ; return nil end
	if args[1] == "spawnall" then
		local cursor = Cyclone.cursor
		for _,item in ipairs(Cyclone.nsh.all("Item")) do
			if (not net.online) or net.host then
				item:create(cursor.x, cursor.y)
			else
				spawn_client:sendAsClient(item:getObject(), cursor.x, cursor.y)
			end
		end
		return nil
	elseif args[1] == "tier" then
		local count = tonumber(args[3]) or 1
		local pool = Cyclone.nsh.find(args[2], "ItemPool")
		if pool then
			local crate = pool:getCrate()
			if crate then
				local player = net.localPlayer or misc.players[1]
				for i=1,count do
					if (not net.online) or net.host then
						crate:create(player.x, player.y)
					else
						spawn_client:sendAsClient(crate, player.x, player.y)
					end
					Cyclone.print(string.format("Spawned %s crate", pool:getName()))
				end
			else
				Cyclone.error(string.format("item: ItemPool %s doesn't have a crate", pool:getName()))
			end
		else
			Cyclone.error("item: " .. Cyclone.nsh.error(args[2], "ItemPool"))
		end
		return nil
	else
		local item, count, player = itemParse(args[2], args[3], args[4])
		if not item then return nil end
		if args[1] == "give" then
			item_sync(player:getNetIdentity(), item, count)
		elseif args[1] == "remove" or args[1] == "fremove" then
			if (not modloader.checkMod("item-removal-lib")) then
				Cyclone.error("item: IRL not found.")
				return nil
			end
			if not itemremover.getRemoval(item) then
				Cyclone.error(string.format("item: Item %s is missing removal function", item:getName()))
				return nil
			end
			for i=1,count do
				itemremover.removeItem(player, item, (args[1] == "fremove"))
			end
		elseif args[1] == "rawremove" then
			remove_sync(player:getNetIdentity(), item, count)
		elseif args[1] == "spawn" or args[1] == "qspawn" then
			local cursor = Cyclone.cursor
			for i=1,count do
				if (not net.online) or net.host then
					local i_item = item:create(cursor.x, cursor.y)
					if args[1] == "qspawn" then
						i_item:setAlarm(0, -1)
					end
				else
					spawn_client:sendAsClient(item:getObject(), cursor.x, cursor.y)
				end
			end
		end
	end
end)