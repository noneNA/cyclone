-- Cyclone - contents/commands/burst.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.nsh

Cyclone.Terminal.addCommand("burst", function(args)
	if args[1] == "find" then
		Cyclone.print(Cyclone.nsh.suggestions(args[2], "ParticleType", ", "))
	else
		local particle = Cyclone.nsh.find(args[1], "ParticleType")
		if not particle then
			Cyclone.error("burst: " .. Cyclone.nsh.error(args[1], "ParticleType"))
			return nil
		end
		local particle_count = tonumber(args[2]) or 1
		particle:burst("above", Cyclone.cursor.x, Cyclone.cursor.y, particle_count)
	end
end)