-- Cyclone - contents/commands/aliases.lua

-- Dependencies:
---- Cyclone.Terminal

local ALIASES = {
	"spawn", "object spawn",
	"give", "item give",
	"tpt", "teleport teleporter",
	"tp", "teleport",
	"su", "superuser",
	"card", "monstercard spawn",
	"unlock", "achievement unlockall",
	"elite", "elitetype make",
}

local i = 1
while i <= #ALIASES - 1 do
	local name, alias = ALIASES[i], ALIASES[i + 1]
	Cyclone.Terminal.addAlias(name, alias)
	i = i + 2
end