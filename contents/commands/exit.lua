-- Cyclone - contents/commands/exit.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.Terminal_GUI

Cyclone.Terminal.addCommand("exit", function()
	Cyclone.Terminal_GUI.close()
end)