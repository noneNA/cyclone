-- Cyclone - contents/commands/selection.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.info

-- If you know where I can get these from the code that would be appreciated
local ITEM_COLORS = {
	["w"]  = Color.fromRGB(255, 255, 255),
	["g"]  = Color.fromRGB(147, 225, 128),
	["r"]  = Color.fromRGB(231, 84, 58),
	["or"] = Color.fromRGB(255, 128, 0),
	["p"]  = Color.fromRGB(172, 114, 194),
}

Cyclone.Terminal.addCommand("player", function(args)
	if args[1] == nil then
		for _,player in ipairs(misc.players) do
			Cyclone.print(Cyclone.info(player))
		end
	else
		if args[1] == "items" then
			local player = misc.players[tonumber(args[2])] or net.localPlayer or misc.players[1]
			for _,item in ipairs(Cyclone.nsh.all("Item")) do
				local count = player:countItem(item)
				if count and (count > 0) then
					local color = ITEM_COLORS[item.color] or item.color
					Cyclone.print(string.format("%s (x%s)", item:getName(), count), color)
				end
			end
		end
	end
end)