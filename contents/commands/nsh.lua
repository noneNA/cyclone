-- Cyclone - contents/commands/nsh.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.nsh

Cyclone.Terminal.addCommand("nsh", function(args)
	if args[1] == "find" then
		local status, message = pcall(Cyclone.nsh.suggestions, args[2], args[3])
		if status then
			Cyclone.print(message)
		else
			Cyclone.error("nsh: Couldn't get suggestions")
			Cyclone.error("nsh: Reason: " .. message)
		end
	elseif args[1] == "namespace" then
		local namespace = Cyclone.nsh.getNamespace()
		if namespace then
			Cyclone.print(string.format("Current namespace with priority is %s", namespace))
		else
			Cyclone.print("No namespace has priority")
		end
	end
end)