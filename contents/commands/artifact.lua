-- Cyclone - contents/commands/artifact.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.nsh
---- CycloneLib.net
---- Cyclone.superuser

local artifact_sync = CycloneLib.net.AllPacket("artifact", function(name, origin, state)
	local artifact = Artifact.find(name, origin)
	artifact.active = state
end)

local spawn_client = net.Packet.new("artifact", function(sender, object, x, y)
	object:create(x, y)
end)

Cyclone.nsh.newCommand("artifact", "Artifact", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("artifact: You don't have superuser")
		return nil
	end
	
	if args[1] == "spawn" then
		local artifact = Cyclone.nsh.find(args[2], "Artifact")
		if not artifact then
			Cyclone.error("artifact: " .. Cyclone.nsh.error(args[2], "Artifact"))
			return nil
		end
		local cursor = Cyclone.cursor
		local object = artifact:getObject()
		if object then
			if (not net.online) or net.host then
				object:create(cursor.x, cursor.y)
			else
				spawn_client:sendAsClient(object, cursor.x, cursor.y)
			end
		else
			Cyclone.error(string.format("artifact: Artifact %s doesn't have an object", artifact:getName()))
		end
	else
		local artifact = Cyclone.nsh.find(args[1], "Artifact")
		if artifact then
			local new_state
			if args[2] == "on" then new_state = true
			elseif args[2] == "off" then new_state = false
			else new_state = not artifact.active end
			artifact_sync(artifact:getName(), artifact:getOrigin(), new_state)
			Cyclone.print(string.format("Artifact %s is now %s", artifact:getName(), new_state and "active" or "inactive"))
		else
			Cyclone.error("artifact: " .. Cyclone.nsh.error(args[1], "Artifact"))
		end
	end
end)