-- Cyclone - contents/commands/kill.lua

-- Dependencies:
---- Cyclone.Terminal
---- CycloneLib.net
---- Cyclone.superuser

local kill_sync = CycloneLib.net.AllPacket("kill", function(net_player)
	local player = net_player:resolve()
	if player then
		player:kill()
	end
end)

Cyclone.Terminal.addCommand("kill", function(args)
	if args[1] == "all" then
		if not Cyclone.superuser() then
			Cyclone.error("kill: You don't have superuser")
			return nil
		end
		for _,player in ipairs(misc.players) do
			kill_sync(player:getNetIdentity())
		end
	else
		local player = misc.players[tonumber(args[1])]
		if not player then
			player = net.localPlayer
		end
		if not player then
			player = misc.players[1]
		end
		kill_sync(player:getNetIdentity())
	end
end)