-- Cyclone - contents/commands/get.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor

Cyclone.Terminal.addCommand("get", function(args)
	local key = args[1] or ""
	for _,instance in ipairs(Cyclone.cursor.selection) do
		Cyclone.print(string.format("ID=%s Name=%s %s=%s",
			instance.id,
			instance:getObject():getName(),
			key,
			instance:get(key)
		))
	end
end)