-- Cyclone - contents/commands/superuser.lua

-- Dependencies
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("superuser", function(args)
	if args[1] == "check" then
		Cyclone.print(Cyclone.superuser())
	elseif args[1] == "grant" or args[1] == "deny" or args[1] == "get" then
		if Cyclone.superuser() then
			local player = misc.players[tonumber(args[2])]
			if not player then
				for _,instance in ipairs(Cyclone.cursor.selection) do
					if isa(instance, "PlayerInstance") then
						player = instance
						break
					end
				end
			end
			if not player then
				Cyclone.error("superuser: Player couldn't be found. Either select or give a player index.")
				return nil
			end
			if args[1] == "grant" then
				Cyclone.superuser.grant(player)
				Cyclone.print("Player has been granted superuser")
			elseif args[1] == "deny" then
				Cyclone.superuser.deny(player)
				Cyclone.print("Player has been denied superuser")
			elseif args[1] == "get" then
				Cyclone.print(Cyclone.superuser.get(player))
			end
		else
			Cyclone.error("superuser: You don't have superuser")
		end
	end
end)