-- Cyclone - contents/commands/elite.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.nsh
---- CycloneLib.net
---- Cyclone.superuser

local ELITE_PRIMES = {
	[EliteType.find("Blazing")] = 3,
	[EliteType.find("Frenzied")] = 5,
	[EliteType.find("Leeching")] = 7,
	[EliteType.find("Overloading")] = 11,
	[EliteType.find("Volatile")] = 13,
}

local function getElitePrime(elite_type)
	local prime = ELITE_PRIMES[elite_type]
	return prime and prime or 1
end

local elite_packet = net.Packet.new("elite_command", function(sender, net_enemy, elite_name, elite_namespace)
	local elite = EliteType.find(elite_name, elite_namespace)
	local i_enemy = net_enemy:resolve()
	if elite and i_enemy then
		i_enemy:makeElite(elite)
	end
end)

local elite_sync = CycloneLib.net.AllPacket("elite", function(net_instance, elite_name, elite_namespace)
	local instance = net_instance:resolve()
	if instance then
		local elite = EliteType.find(elite_name, elite_namespace)
		instance:makeElite(elite)
	end
end)

local blight_sync = CycloneLib.net.AllPacket("blight", function(net_instance, blight_type)
	local instance = net_instance:resolve()
	if instance then
		instance:makeBlighted(blight_type)
	end
end)

Cyclone.nsh.newCommand("elitetype", "EliteType", function(args)
	if args[1] == "make" then
		if not Cyclone.superuser() then
			Cyclone.error("elitetype: You don't have superuser")
			return nil
		end
		
		local elite_types = {}
		for i=2,#args do
			local elite_type = Cyclone.nsh.find(args[i], "EliteType")
			if elite_type then
				table.insert(elite_types, elite_type)
			else
				Cyclone.error("elitetype: " .. Cyclone.nsh.error(args[i], "EliteType"))
			end
		end
		
		if #elite_types == 0 then
			Cyclone.error("elitetype: Couldn't find any EliteType with the provided names")
			return nil
		end
		
		local blight_type
		if #elite_types > 1 then
			blight_type = 1
			for _,elite_type in ipairs(elite_types) do
				blight_type = blight_type * getElitePrime(elite_type)
			end
		end
		
		for _, instance in ipairs(Cyclone.cursor.selection) do
			if isa(instance, "ActorInstance") then
				if not blight_type then
					local elite_type = elite_types[1]
					if net.online then
						elite_sync(instance:getNetIdentity(), elite_type:getName(), elite_type:getOrigin())
					elseif not instance:makeElite(elite_type) then
						Cyclone.error(string.format("elitetype: Couldn't apply EliteType to actor %s", instance:getObject():getName()))
					end
				else
					if net.online then
						blight_sync(instance:getNetIdentity(), blight_type)
					elseif not instance:makeBlighted(blight_type) then
						Cyclone.error(string.format("elitetype: Couldn't make actor %s blight", instance:getObject():getName()))
					end
				end
			end
		end
		
		if blight_type then
			local elite_string = ""
			for _,elite_type in ipairs(elite_types) do
				elite_string = elite_string .. ((#elite_string == 0) and "" or ", ") .. elite_type:getName()
			end
			Cyclone.print(string.format("Made the selection blighted with types %s", elite_string))
		else
			Cyclone.print(string.format("Made the selection elite with type %s", elite_types[1]:getName()))
		end
	end
end)