-- Cyclone - contents/commands/unlock.lua

-- Dependencies
---- Cyclone.Terminal
---- CycloneLib.modloader
---- Cyclone.nsh
---- Cyclone.superuser

local function unlockAchievement(achievement)
	if not achievement:isComplete() then
		achievement:increment(achievement.requirement)
	end
end

Cyclone.nsh.newCommand("achievement", "Achievement", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("achievement: You don't have superuser")
		return nil
	end
	
	if args[1] == "unlockall" then
		--Vanilla doesn't work
		--for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,namespace in ipairs(modloader.getMods()) do
			for _,achievement in ipairs(Achievement.findAll(namespace)) do
				unlockAchievement(achievement)
			end
		end
	elseif args[1] == "unlock" then
		local i = 2
		while i <= #args do
			local achievement = Cyclone.nsh.find(args[i], "Achievement")
			if achievement then
				if achievement:getOrigin() ~= "Vanilla" then
					unlockAchievement(achievement)
				else
					Cyclone.error(string.format("unlock: Vanilla achievements can't be unlocked (%s)", achievement:getName()))
				end
			else
				Cyclone.error("unlock: " .. Cyclone.nsh.error(args[i], "Achievement"))
			end
			i = i + 1
		end
	end
end)