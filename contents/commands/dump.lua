-- Cyclone - contents/commands/dump.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.table

Cyclone.Terminal.addCommand("dump", function(args)
	local selection = Cyclone.cursor.selection
	if #selection <= 0 then
		if not net.online then
			local new_args = {}
			for i=1,#args do
				table.insert(new_args, args[i])
			end
			Cyclone.Terminal.run("object select " .. CycloneLib.table.listString(new_args, " "))
		else
			Cyclone.error("dump: Nothing selected and on multiplayer")
		end
	end
	
	selection = Cyclone.cursor.selection
	if #selection <= 0 then
		Cyclone.error("dump: Nothing selected or to be spawned")
	else
		for _,instance in ipairs(selection) do
			instance:dumpVariables(true, true)
			Cyclone.print(string.format("Instance of %s dumped", instance:getObject():getName()))
		end
	end
end)