-- Cyclone - contents/commands/echo.lua

-- Dependencies
---- Cyclone.Terminal
---- CycloneLib.table

Cyclone.Terminal.addCommand("echo", function(args)
	if #args > 0 then
		Cyclone.print(CycloneLib.table.listString(args, " ", true))
	end
end)