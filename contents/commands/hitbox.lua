-- Cyclone - contents/commands/hitbox.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.toggler

local store = Cyclone.toggler.new("hitbox", "Visible hitboxes", false)

local LIFE = 4
local explosions = {}
local o_explosion = Object.find("Explosion")
callback.register("onDraw", function()
	if store.state then
		for _,i_explosion in ipairs(o_explosion:findAll()) do
			local r_explosion = CycloneLib.Rectangle.new(i_explosion)
			explosions[r_explosion] = LIFE
		end
		
		for r_explosion,life in pairs(explosions) do
			if life <= 0 then
				explosions[r_explosion] = nil
			else
				r_explosion:draw(Color.RED, 0.4)
				explosions[r_explosion] = life - 1
			end
		end
	end
end)