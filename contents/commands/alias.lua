-- Cyclone - contents/commands/list.lua

-- Dependencies:
---- Cyclone.Terminal
---- CycloneLib.table

Cyclone.Terminal.addCommand("alias", function(args)
	if not args[1] then
		local aliases = Cyclone.Terminal.getAliases()
		for name,alias in pairs(aliases) do
			Cyclone.print(string.format("%q -> %q", name, alias))
		end
	else
		if args[2] then
			Cyclone.Terminal.addAlias(args[1], args[2])
			Cyclone.print(string.format("Aliased %q to %q", args[1], args[2]))
		else
			Cyclone.Terminal.removeAlias(args[1])
			Cyclone.print(string.format("Removed alias %q", args[1]))
		end
	end
end)