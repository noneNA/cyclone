-- Cyclone - contents/commands/timescale.lua

-- Dependencies
---- Cyclone.Terminal
---- CycloneLib.net
---- Cyclone.superuser

local MAX = 60

local prev_alarm = MAX
local timescale = 1
local function resetTimeScale() timescale = 1 ; prev_alarm = MAX end
callback.register("onGameEnd", resetTimeScale)

local timescale_sync = CycloneLib.net.AllPacket("timescale", function(value)
	timescale = value
end)

callback.register("onStep", function()
	local alarm_cap = MAX / timescale
	if (misc.director:getAlarm(0) == MAX) and (prev_alarm == 1) then
		misc.director:setAlarm(0, alarm_cap)
	end
	prev_alarm = misc.director:getAlarm(0)
end)

Cyclone.Terminal.addCommand("timescale", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("timescale: You don't have superuser")
		return nil
	end
	
	local scale = tonumber(args[1])
	if scale then
		scale = math.min(scale, MAX)
		timescale_sync(scale)
		Cyclone.print(string.format("Timescale set to '%s'", scale))
	else
		timescale_sync(1)
		Cyclone.print("Timescale reset")
	end
end)