-- Cyclone - content/commands/gui.lua

-- Dependencies:
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("gui", function(args)
	if args[1] == "move" then
		Cyclone.Terminal_GUI.move(tonumber(args[2]) or 0, tonumber(args[3]) or 0)
	elseif args[1] == "shift" then
		Cyclone.Terminal_GUI.move(tonumber(args[2]) or 0, tonumber(args[3]) or 0, true)
	elseif args[1] == "resize" then
		Cyclone.Terminal_GUI.resize(tonumber(args[2]) or -1, tonumber(args[3]) or -1)
	elseif args[1] == "reset" then
		Cyclone.Terminal_GUI.reset()
	else Cyclone.error(string.format("gui: Subcommand %s not found", args[1])) end
end)