-- Cyclone - contents/commands/log.lua

-- Dependencies
---- Cyclone.Terminal

local function enable()
	Cyclone.Terminal.setLogging(true)
end

local function disable()
	Cyclone.Terminal.setLogging(false)
end

local store = Cyclone.toggler.new("log", "Logging", false, enable, disable)