-- Cyclone - contents/commands/teleporter.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.superuser

--[[
Thanks to Saturn for the explanation below:
1 : Start timer
2 : Kill remaining enemies
3 : Finished teleporter event and enemies killed
4 : Teleport to the next level
5 : Teleport to the last level
--]]

local o_teleporter = Object.find("Teleporter", "vanilla")

local tele_sync = CycloneLib.net.AllPacket("next_stage", function(net_teleporter, active)
	local i_teleporter = net_teleporter:resolve()
	if i_teleporter and (i_teleporter:get("active") < active) then
		i_teleporter:set("active", active)
	end
end)

local cycle_charges = 0
local function resetCycleCharges() cycle_charges = 0 end
callback.register("onGameStart", resetCycleCharges)

local cycle_sync = CycloneLib.net.AllPacket("teleporter_cycle", function(cycles)
	cycle_charges = cycles
end)

callback.register("onStageEntry", function()
	if net.host then
		if cycle_charges > 0 then
			cycle_sync(cycle_charges - 1)
		end
	end
end)

callback.register("onStep", function()
	if net.host then
		if cycle_charges > 0 then
			local i_teleporter = o_teleporter:find(1)
			if not i_teleporter then
				cycle_sync(0)
				return nil
			end
			tele_sync(i_teleporter:getNetIdentity(), 4)
		end
	end
end)

Cyclone.Terminal.addCommand("teleporter", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("teleporter: You don't have superuser")
		return nil
	end
	
	local i_teleporter, net_teleporter = o_teleporter:find(1), nil
	if not i_teleporter then
		Cyclone.error("teleporter: Teleporter not found.")
		return nil
	else
		net_teleporter = i_teleporter:getNetIdentity()
	end
	
	if args[1] == "finish" then
		tele_sync(net_teleporter, 3)
	elseif args[1] == "next" then
		tele_sync(net_teleporter, 4)
	--[[
	elseif args[1] == "last" then
		teleporter_instance:set("active", 5)
	--]]
	elseif args[1] == "cycle" then
		local _charges = cycle_charges + (tonumber(args[2]) or 0)
		cycle_sync(_charges)
	end
end)