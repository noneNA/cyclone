-- Cyclone - contents/commands/damage

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.superuser

local EXPLOSION_SIZE = 16
local DEFAULT_DAMAGE = math.pow(2,16)

local damage_sync = CycloneLib.net.AllPacket("damage", function(x, y, damage)
	misc.fireExplosion(x, y, EXPLOSION_SIZE/(19*2), EXPLOSION_SIZE/(4*2), damage, "neutral")
end)

Cyclone.Terminal.addCommand("damage", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("damage: You don't have superuser")
		return nil
	end
	
	local cursor = Cyclone.cursor
	damage_sync(cursor.x, cursor.y, tonumber(args[1]) or DEFAULT_DAMAGE)
end)