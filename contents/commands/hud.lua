-- Cyclone - contents/commands/hud.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.toggler

local HUD_VARIABLES = {
	["show_items"] = 0,
	["show_time"] = 0,
	["show_gold"] = 0,
	["show_skills"] = 0,
	["show_level_name"] = 0,
	["show_boss"] = 0,
	["artifact_lock_choice_alpha"] = 0,
	["objective_text"] = "",
}

local hud_cache = {}
local function resetHudState() hud_cache = {} end
callback.register("onGameEnd", resetHudState)

local function enable()
	for key, value in pairs(HUD_VARIABLES) do
		hud_cache[key] = misc.hud:get(key)
		misc.hud:set(key, value)
	end
end

local function disable()
	for key, value in pairs(hud_cache) do
		misc.hud:set(key, value)
	end
end

local store = Cyclone.toggler.new("hud", "No HUD mode", false, enable, disable)