-- Cyclone - contents/commands/random.lua

-- Dependencies
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("random", function(args)
	local min_value = tonumber(args[1])
	local max_value = tonumber(args[2])
	if not max_value then max_value = min_value or 1 end
	Cyclone.print(math.random(min_value or 0, max_value))
end)