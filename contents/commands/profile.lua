-- Cyclone - contents/commands/profile.lua

-- Dependencies
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("profile", function()
	Cyclone.print(modloader.getProfileName())
end)