-- Cyclone - contents/commands/selection.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.info

Cyclone.Terminal.addCommand("selection", function(args)
	if args[1] == nil then
		for i,instance in ipairs(Cyclone.cursor.selection) do
			Cyclone.print(string.format("[%i] %s", i, Cyclone.info(instance)))
		end
	else
		if args[1] == "remove" then
			local index = tonumber(args[2])
			if (not index) or (not Cyclone.cursor._selection[index]) then
				Cyclone.error(string.format("selection: Index %q could not be found", args[2]))
				return nil
			end
			table.remove(Cyclone.cursor._selection, index)
		end
	end
end)