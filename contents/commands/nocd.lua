-- Cyclone - contents/commands/nocd.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.toggler

local nocd_sync = CycloneLib.net.AllPacket("nocd", function(net_player, state)
	local player = net_player:resolve()
	if player then
		player:getData().nocd = state
	end
end)

local function enable()
	local player = net.localPlayer or misc.players[1]
	nocd_sync(player:getNetIdentity(), true)
end

local function disable()
	local player = net.localPlayer or misc.players[1]
	nocd_sync(player:getNetIdentity(), false)
end

local store = Cyclone.toggler.new("nocd", "No cooldown mode", false, enable, disable, true)

callback.register("onPlayerStep", function(player)
	if player:getData().nocd then
		for i=2,5 do
			player:setAlarm(i + 1, -1)
		end
		player:setAlarm(0, -1)
	end
end)