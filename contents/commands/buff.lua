-- Cyclone - contents/commands/buff.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.nsh
---- CycloneLib.net
---- Cyclone.superuser

local buff_sync = CycloneLib.net.AllPacket("buff", function(net_instance, name, origin, duration)
	local instance = net_instance:resolve()
	if instance then
		local buff = Buff.find(name, origin)
		instance:applyBuff(buff, duration)
	end
end)

local DURATION = 300

Cyclone.nsh.newCommand("buff", "Buff", function(args)
	if false then
		
	else
		if not Cyclone.superuser() then
			Cyclone.error("buff: You don't have superuser")
			return nil
		end
		
		local min_index = (args[1] == "apply") and 2 or 1
		local max_index = #args
		
		local duration = tonumber(args[#args])
		if duration then
			duration = duration * 60
			max_index = max_index - 1
		else
			duration = DURATION
		end
		
		local buffs = {}
		for i=min_index,max_index do
			local buff = Cyclone.nsh.find(args[i], "Buff")
			if buff then
				table.insert(buffs, buff)
			else
				Cyclone.error("buff: " ..Cyclone.nsh.error(args[i], "Buff"))
			end
		end
		
		if #buffs == 0 then
			Cyclone.error("buff: No buff with the given names were found")
			return nil
		end
		
		local instances
		if #Cyclone.cursor.selection > 0 then
			instances = Cyclone.cursor.selection
		else
			instances = misc.players
		end
		for _,instance in ipairs(instances) do
			if isa(instance, "ActorInstance") then
				for _,buff in ipairs(buffs) do
					-- Getting name and origin is slower so checking online is probably faster
					if net.online then
						buff_sync(instance:getNetIdentity(), buff:getName(), buff:getOrigin(), duration)
					else
						instance:applyBuff(buff, duration)
					end
				end
			end
		end
		Cyclone.print(string.format("Buffs applied for %s seconds", duration / 60))
	end
end)