-- Cyclone - contents/commands/flags.lua

-- Dependencies
---- Cyclone.Terminal

local FLAGS_TEXT = [[
Current flags (%s):
	%s
]]

Cyclone.Terminal.addCommand("flags", function()
	local flag_string = ""
	local flags = modloader.getFlags()
	for _,flag_name in ipairs(flags) do
		flag_string = string.format(
			"%s%s%s",
			flag_string,
			flag_string == "" and "" or "\n\t",
			flag_name
		)
	end
	Cyclone.print(string.format(FLAGS_TEXT, #flags, flag_string))
end)