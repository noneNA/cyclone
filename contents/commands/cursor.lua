-- Cyclone - contents/commands/cursor.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor

Cyclone.Terminal.addCommand("cursor", function(args)
	local cursor = Cyclone.cursor
	if args[1] == "shift" then
		local dx, dy = tonumber(args[2]) or 0, tonumber(args[3]) or 0
		cursor.x, cursor.y = cursor.x + dx, cursor.y + dy
	elseif args[1] == "player" then
		cursor.x, cursor.y = misc.players[1].x, misc.players[1].y
	else
		cursor.x, cursor.y = tonumber(args[1]) or 0, tonumber(args[2]) or 0
		Cyclone.print(cursor.x, cursor.y)
	end
end)