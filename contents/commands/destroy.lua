-- Cyclone - contents/commands/destroy.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.superuser
---- CycloneLib.net

local destroy_sync = CycloneLib.net.AllPacket("destroy", function(net_instance)
	local instance = net_instance:resolve()
	if instance then
		instance:destroy()
	end
end)

Cyclone.Terminal.addCommand("destroy", function()
	if not Cyclone.superuser() then
		Cyclone.error("destroy: You don't have superuser")
		return nil
	end
	
	for _,instance in ipairs(Cyclone.cursor.selection) do
		if net.online then
			local status, message = pcall(instance.getNetIdentity, instance)
			if status then
				destroy_sync(instance:getNetIdentity())
			else
				Cyclone.error("destroy: Couldn't destroy")
				Cyclone.error("destroy: Reason: " .. message)
			end
		else
			local status, message = pcall(instance.destroy, instance)
			if not status then Cyclone.error(string.format("destroy: %s (%s)", message, instance:getObject():getName())) end
		end
	end
end)