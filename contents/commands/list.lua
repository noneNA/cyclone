-- Cyclone - contents/commands/list.lua

-- Dependencies:
---- Cyclone.Terminal
---- CycloneLib.table

Cyclone.Terminal.addCommand("list", function()
	local commands = CycloneLib.table.swap(Cyclone.Terminal.getCommands())
	Cyclone.print(CycloneLib.table.listString(commands, ", "))
end)