-- Cyclone - contents/commands/god.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.toggler
---- CycloneLib.net

local BIG = 2^2^2^2^2

local GOD_VARIABLES = {
	pHmax = 4.2,
	maxhp_base = BIG,
	feather = BIG,
	hp_regen = BIG,
}

local god_sync = CycloneLib.net.AllPacket("god", function(net_player, state)
	local player = net_player:resolve()
	if player then
		local d_player = player:getData()
		local old_state = d_player.god
		if state and (not old_state) then
			d_player.god_state = {}
			for key,value in pairs(GOD_VARIABLES) do
				d_player.god_state[key] = player:get(key)
				player:set(key, value)
			end
			d_player.god = true
		elseif (not state) and (old_state) then
			for key,value in pairs(GOD_VARIABLES) do
				player:set(key, d_player.god_state[key])
			end
			d_player.god_state = nil
			d_player.god = false
		end
	end
end)

local function enable()
	local player = net.localPlayer or misc.players[1]
	god_sync(player:getNetIdentity(), true)
end

local function disable()
	local player = net.localPlayer or misc.players[1]
	god_sync(player:getNetIdentity(), false)
end

local store = Cyclone.toggler.new("god", "God mode", false, enable, disable, true)