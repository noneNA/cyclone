-- Cyclone - contents/commands/camera.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor

local camera_override = {}
local function resetCameraOverride() camera_override = {} end
callback.register("onCameraUpdate", function()
	if camera_override.y then
		camera.y = camera_override.y - camera.height/2
	end
	if camera_override.x then
		camera.x = camera_override.x - camera.width/2
	end
end)

Cyclone.Terminal.addCommand("camera", function(args)
	Cyclone.print("Current camera ", camera.x, camera.y)
	if args[1] == "reset" then
		camera_override = {}
	elseif args[1] == "player" then
		local player = misc.players[1]
		camera_override.x, camera_override.y = player.x, player.y
	elseif args[1] == "cursor" then
		local cursor = Cyclone.cursor
		camera_override.x, camera_override.y = cursor.x, cursor.y
	else
		camera_override.x, camera_override.y = tonumber(args[1]) or 0 + camera.x, tonumber(args[2]) or 0 + camera.y
	end
	Cyclone.print(string.format("Camera moved to %s %s", camera_override.x, camera_override.y))
end)