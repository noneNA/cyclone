-- Cyclone - contents/commands/seed.lua

-- Dependencies
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("seed", function(args)
	local seed = tonumber(args[1])
	local status, message = pcall(misc.setRunSeed, seed)
	if not seed then
		Cyclone.error("seed: No seed specified")
		return nil
	end
	if status then
		Cyclone.print(string.format("Next run will have seed %s", seed))
	else
		Cyclone.error("seed: Couldn't set seed")
		Cyclone.error(string.format("seed: Reason: '%s'", message))
	end
end)