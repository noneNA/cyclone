-- Cyclone - contents/commands/help.lua

-- Dependencies
---- Cyclone.Terminal

local HELP = [[
Position the cursor by right-clicking
Some useful commands:
	help: This command
	list: Lists all commands available
	item: Used to manipulate items
		item give hoof
		item find sy
		item tier uncommon
	spawn: Used to spawn object at the location of the cursor
		spawn lizard
		spawn chest1
]]

Cyclone.Terminal.addCommand("help", function()
	Cyclone.print(HELP)
end)