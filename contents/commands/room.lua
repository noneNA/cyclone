-- Cyclone - contents/commands/room.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.nsh
---- Cyclone.superuser
---- CycloneLib.net

-- GML side doesn't like this
--[[
local stage = Stage.new("room")
local restore_stage = nil
callback.register("onStageEntry", function()
	local restore_room = stage.rooms[1]
	for _,room in ipairs(stage.rooms:toTable()) do
		stage.rooms:remove(room)
	end
	if restore_stage then
		restore_stage.rooms:add(restore_room)
		restore_stage = nil
	end
end)

Cyclone.Terminal.addCommand("room", function(args)
	for _,room in ipairs(Room.findAll("vanilla")) do
		Cyclone.print(room:getName())
	end
	local room_name = args[1] or "1_1_1"
	local room = Room.find(room_name)
	if not room then
		Cyclone.error(string.format("room: Room '%s' not found", room_name))
		return nil
	end
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,stage in ipairs(Stage.findAll(namespace)) do
			if stage.rooms:contains(room) then
				restore_stage = stage
				break
			end
		end
		if restore_stage then break end
	end
	if restore_stage then
		restore_stage.rooms:remove(room)
	end
	stage.rooms:add(room)
	Stage.transport(stage)
end)
--]]

local stage = Stage.new("room")
local restore_stage
local restore_rooms
callback.register("onStageEntry", function()
	if restore_stage then
		CycloneLib.list.removeAll(restore_stage.rooms)
		for _,room in ipairs(restore_rooms) do
			restore_stage.rooms:add(room)
		end
		restore_stage = nil
		restore_rooms = nil
	end
	CycloneLib.list.removeAll(stage.rooms)
end)

local room_sync = CycloneLib.net.AllPacket("room", function(name, origin)
	local room = Room.find(name, origin)
	
	local destination
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,stage in ipairs(Stage.findAll(namespace)) do
			if stage.rooms:contains(room) then
				destination = stage
				break
			end
		end
		if destination then break end
	end
	
	if destination then
		restore_stage = destination
		restore_rooms = destination.rooms:toTable()
	else
		destination = stage
	end
	
	CycloneLib.list.removeAll(destination.rooms)
	destination.rooms:add(room)
	Stage.transport(destination)
end)

Cyclone.nsh.newCommand("room", "Room", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("room: You don't have superuser")
		return nil
	end

	local room = Cyclone.nsh.find(args[1], "Room")
	if not room then
		Cyclone.error("room: " .. Cyclone.nsh.error(args[1], "Room"))
		return nil
	end
	
	room_sync(room:getName(), room:getOrigin())
end)