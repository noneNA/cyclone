-- Cyclone - contents/commands/difficulty.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.nsh
---- CycloneLib.net
---- Cyclone.superuser

local difficulty_sync = CycloneLib.net.AllPacket("difficulty", function(name, origin)
	local difficulty = Difficulty.find(name, origin)
	Difficulty.setActive(difficulty)
end)

Cyclone.nsh.newCommand("difficulty", "Difficulty", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("difficulty: You don't have superuser")
		return nil
	end
	
	local difficulty = Cyclone.nsh.find(args[1], "Difficulty")
	if not difficulty then
		Cyclone.error("difficulty: " .. Cyclone.nsh.error(args[1], "Difficulty"))
		return nil
	end
	difficulty_sync(difficulty:getName(), difficulty:getOrigin())
	Cyclone.print(string.format("Difficulty set to %s", difficulty:getName()))
end)