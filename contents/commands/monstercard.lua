-- Cyclone - contents/commands/monstercard.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.nsh
---- Cyclone.superuser

local o_spawn = Object.find("Spawn")
local s_mask = Sprite.find("NoMask")
local s_lemurian = Sprite.find("LizardSpawn")
local mc_lemurian = MonsterCard.find("Lemurian")

local card_sync = CycloneLib.net.AllPacket("card", function(name, origin, x, y, elite_name, elite_origin)
	local card = MonsterCard.find(name, origin)
	local elite = EliteType.find(elite_name or "", elite_origin)

	local i_spawn = o_spawn:create(x, y)
	i_spawn:set("child", card.object.id)
	if (card.sprite ~= s_lemurian) or (card == mc_lemurian) then
		i_spawn.sprite = card.sprite
	else
		i_spawn.sprite = s_mask
		--i_spawn.visible = false
	end
	if card.sound then
		i_spawn:set("sound_spawn", card.sound.id)
	end
	
	if elite then
		i_spawn:set("elite_type", elite.id)
		i_spawn:set("prefix_type", 1)
	end
end)

Cyclone.nsh.newCommand("monstercard", "MonsterCard", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("monstercard: You don't have superuser")
		return nil
	end

	if args[1] == "spawn" then
		local card = Cyclone.nsh.find(args[2], "MonsterCard")
		if card then				
			local elite = Cyclone.nsh.find(args[3], "EliteType")
			
			local elite_name, elite_origin
			if elite then
				elite_name, elite_origin = elite:getName(), elite:getOrigin()
			end
			
			card_sync(card:getName(), card:getOrigin(), Cyclone.cursor.x, Cyclone.cursor.y, elite_name, elite_origin)
			
			if elite then
				Cyclone.print(string.format("Spawned %s with EliteType %s", card:getName(), elite:getName()))
			else
				Cyclone.print(string.format("Spawned %s", card:getName()))
			end
		else
			Cyclone.error("monstercard: " .. Cyclone.nsh.error(args[1], "MonsterCard"))
		end
	end
end)