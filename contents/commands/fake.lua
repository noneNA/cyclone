-- Cyclone - contents/commands/fake.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.toggler

local store = Cyclone.toggler.new("fake", "Fake damage", true, nil, nil, true, true)
callback.register("onFire", function(damager)
	if not store.state then
		damager:set("damage_fake", damager:get("damage"))
	end
end)