-- Cyclone - contents/commands/object.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.nsh
---- Cyclone.superuser
---- Cyclone.cursor
---- CycloneLib.net
---- CycloneLib.table
---- Cyclone.info

local HOST_SYNC = {}
do
	for _,artifact in ipairs(Cyclone.nsh.all("Artifact")) do
		table.insert(HOST_SYNC, artifact:getObject())
	end
	for _,item in ipairs(Cyclone.nsh.all("Item")) do
		table.insert(HOST_SYNC, item:getObject())
	end
	for _,itempool in ipairs(Cyclone.nsh.all("ItemPool")) do
		table.insert(HOST_SYNC, itempool:getCrate())
	end
	table.insert(HOST_SYNC, Object.find("GolemG"))
	table.insert(HOST_SYNC, Object.find("JellyG2"))
end
HOST_SYNC = CycloneLib.table.swap(HOST_SYNC)

--[[
local spawn_sync = CycloneLib.net.AllPacket("spawn", function(object, x, y)
	object:create(x, y)
end)
--]]

local host_packet = net.Packet.new("spawn_host", function(sender, object, x, y)
	object:create(x, y)
end)

local client_packet = net.Packet.new("spawn_client", function(sender, object, x, y)
	object:create(x, y)
	if not HOST_SYNC[object] then
		host_packet:sendAsHost(net.ALL, nil, object, x, y)
	end
end)

--[[
Cyclone.Terminal.addCommand("object", function(args)
	if args[1] == "find" or args[1] == "search" then
		Cyclone.print(Cyclone.nsh.suggestions(args[2], "Object"))
	elseif args[1] == "list" or args[1] == "all" then
		Cyclone.print(Cyclone.nsh.suggestions("", "Object"))
--]]
Cyclone.nsh.newCommand("object", "Object", function(args)
	if args[1] == "findall" or args[1] == "selectall" then
		local _select = args[1] == "selectall"
		if _select then
			Cyclone.cursor._selection = {}
		end
		
		local object = Cyclone.nsh.find(args[2], "Object")
		if not object then
			Cyclone.error("object: " .. Cyclone.nsh.error(args[2], "Object"))
			return nil
		end
		
		for _,instance in ipairs(object:findAll()) do
			Cyclone.print(Cyclone.info(instance))
			if _select then
				table.insert(Cyclone.cursor._selection, instance)
			end
		end
	elseif args[1] == "spawn" or args[1] == "select" then
		if not Cyclone.superuser() then
			Cyclone.error("object: You don't have superuser")
			return nil
		end
		
		local _select = args[1] == "select"
		if _select then
			Cyclone.cursor._selection = {}
		end
		local cursor = Cyclone.cursor
		local i = 2
		while i <= #args do
			local object = Cyclone.nsh.find(args[i], "Object")
			if not object then
				Cyclone.error("object: " .. Cyclone.nsh.error(args[i], "Object"))
			else
				local count = tonumber(args[i + 1])
				if count and (not Object.find(args[i + 1])) then
					i = i + 1
				else
					count = 1
				end
				for _i=1,count do
					if not net.online then
						local status, instance = pcall(object.create, object, cursor.x, cursor.y)
						if not status then
							Cyclone.error("object: Couldn't spawn protected object '" .. object:getName() .. "'")
							Cyclone.error("object: Reason: '" .. instance .. "'")
							break
						elseif _select then
							table.insert(Cyclone.cursor._selection, instance)
						end
					else
						local x, y = cursor.x, cursor.y
						if net.host then
							object:create(x, y)
							if not HOST_SYNC[object] then
								host_packet:sendAsHost(net.ALL, nil, object, x, y)
							end
						else
							client_packet:sendAsClient(object, x, y)
						end
					end
				end
			end
			i = i + 1
		end
	end
end)