-- Cyclone - contents/commands/namespace.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.nsh

Cyclone.Terminal.addCommand("namespace", function(args)
	if args[1] == nil then
		Cyclone.nsh.unsetNamespace()
		Cyclone.print("Namespace unset")
	else
		local namespace = Cyclone.nsh.findNamespace(args[1])
		if Cyclone.nsh.setNamespace(namespace, true) then
			Cyclone.print(string.format("The namespace %s now has priority", namespace))
		else
			Cyclone.error(string.format("namespace: Couldn't find namespace with name '%s' (Check the result of the 'mods' command)", args[1]))
		end
	end
end)