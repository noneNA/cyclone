-- Cyclone - contents/commands/clear.lua

-- Dependencies
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("clear", function()
	Cyclone.Terminal.resetOutput()
end)