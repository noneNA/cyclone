-- Cyclone - contents/commands/scan.lua

-- Dependencies:
---- Cyclone.Terminal
---- CycloneLib.modloader
---- Cyclone.cursor
---- Cyclone.info

local SCAN_RADIUS = 8
local function doScan(x,y)
	local instances = {}
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,object in ipairs(Object.findAll(namespace)) do
			for _,instance in pairs(object:findAllEllipse(x - SCAN_RADIUS, y - SCAN_RADIUS, x + SCAN_RADIUS, y + SCAN_RADIUS)) do
				instances[instance] = object
			end
		end
	end
	return instances
end

local function printScan(x,y)
	for k,v in pairs(doScan(x,y)) do
		Cyclone.print(Cyclone.info(k))
		--[[
		Cyclone.print(string.format("ID=%i x,y=%i,%i Name=%q visible=%s",
			k.id,
			k.x,
			k.y,
			v:getName(),
			tostring(k.visible)
		))
		--]]
	end
end

Cyclone.Terminal.addCommand("scan", function(args)
	local x, y
	if args[1] == "cursor" then
		local cursor = Cyclone.cursor
		x, y = cursor.x, cursor.y
	elseif args[1] == "player" then
		local player = misc.players[tonumber(args[2])] or misc.players[1]
		x,y = player.x, player.y
	else
		x, y = tonumber(args[2]), tonumber(args[3])
		if not (x and y) then
			local cursor = Cyclone.cursor
			x, y = cursor.x, cursor.y
		end
	end
	printScan(x, y)
end)