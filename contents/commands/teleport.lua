-- Cyclone - contents/commands/teleport.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.superuser

local teleporter_object = Object.find("Teleporter", "vanilla")

Cyclone.Terminal.addCommand("teleport", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("teleport: You don't have superuser")
		return nil
	end
	local player = misc.players[tonumber(args[2]) or 1]
	if args[1] == "cursor" then
		local cursor = Cyclone.cursor
		player.x, player.y = cursor.x, cursor.y
	elseif args[1] == "teleporter" then
		local teleporter_instance = teleporter_object:find(1)
		if teleporter_instance then player.x = teleporter_instance.x ; player.y = teleporter_instance.y - 7
		else Cyclone.error("teleport: Teleporter not found.") end
	end
end)