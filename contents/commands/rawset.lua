-- Cyclone - contents/commands/rawset.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.cursor
---- Cyclone.superuser
---- CycloneLib.table
---- CycloneLib.net

local ALLOWED_TYPES = {
"nil",
"number",
"string",
}
ALLOWED_TYPES = CycloneLib.table.swap(ALLOWED_TYPES)

local set_sync = CycloneLib.net.AllPacket("rawset", function(net_instance, key, value)
	local instance = net_instance:resolve()
	if instance then
		instance:set(key, value)
	end
end)

Cyclone.Terminal.addCommand("rawset", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("rawset: You don't have superuser")
		return nil
	end
	
	local key = args[1] or ""
	
	local f, message = load(string.format("return %s", args[2]))
	if message then
		Cyclone.error("rawset: Couldn't load")
		Cyclone.error("rawset: Reason: " .. message)
		return nil
	end
	
	local status, value = pcall(f)
	if not status then
		Cyclone.error("rawset: Not a valid lua value")
		Cyclone.error("rawset: Reason: " .. value)
		return nil
	end
	
	local set = false
	for _,instance in ipairs(Cyclone.cursor.selection) do
		if not net.online then
			local status, message = pcall(instance.set, instance, key, value)
			if not status then
				Cyclone.error("rawset: Couldn't set '" .. key .. "'")
				Cyclone.error("rawset: Reason: " .. message)
				return nil
			else
				set = true
			end
		else
			if ALLOWED_TYPES[type(value)] then
				local status, message = pcall(instance.getNetIdentity, instance)
				if status then
					set_sync(instance:getNetIdentity(), key, value)
					set = true
				else
					Cyclone.error("set: Couldn't set '" .. key .. "'")
					Cyclone.error("set: Reason: " .. message)
				end
			end
		end
	end
	
	if not set then return nil end
	Cyclone.print(string.format("Set '%s' to '%s' (%s)", key, value, type(value)))
end)