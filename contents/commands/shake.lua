-- Cyclone - contents/commands/shake.lua

-- Dependencies
---- Cyclone.Terminal

Cyclone.Terminal.addCommand("shake", function(args)
	local amount = tonumber(args[1])
	if amount then
		misc.shakeScreen(amount)
	else
		Cyclone.error("shake: No amount specified")
	end
end)