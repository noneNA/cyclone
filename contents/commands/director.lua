-- Cyclone - contents/commands/director.lua

-- Dependencies:
---- Cyclone.Terminal
---- Cyclone.toggler

local store = Cyclone.toggler.new("director", "Director stopping", false, nil, nil, true, true)
callback.register("onStep", function()
	if store.state then
		misc.director:setAlarm(1, misc.director:getAlarm(1) + 1)
	end
end)