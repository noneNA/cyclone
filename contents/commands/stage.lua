-- Cyclone - contents/commands/stage.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.nsh
---- Cyclone.superuser

local stage_client = net.Packet.new("stage", function(sender, name, origin)
	local stage = Stage.find(name, origin)
	Stage.transport(stage)
end)

Cyclone.nsh.newCommand("stage", "Stage", function(args)
	if not Cyclone.superuser() then
		Cyclone.error("stage: You don't have superuser")
		return nil
	end
	
	local stage = Cyclone.nsh.find(args[1], "Stage")
	if not stage then
		Cyclone.error("stage: " .. Cyclone.nsh.error(args[1], "Stage"))
		return nil
	end
	if stage.rooms:len() <= 0 then
		Cyclone.error(string.format("stage: Stage %s has no rooms", stage:getName()))
		return nil
	end
	
	Cyclone.print(string.format("Going to %s", stage:getName()))
	
	if net.host then
		Stage.transport(stage)
	end
	stage_client:sendAsClient(stage:getName(), stage:getOrigin())
end)