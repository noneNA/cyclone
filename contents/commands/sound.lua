-- Cyclone - contents/commands/sound.lua

-- Dependencies
---- Cyclone.Terminal
---- Cyclone.nsh

Cyclone.Terminal.addCommand("sound", function(args)
	if args[1] == "find" then
		Cyclone.print(Cyclone.nsh.suggestions(args[2], "Sound"))
	else
		local sound = Cyclone.nsh.find(args[1], "Sound")
		if not sound then
			Cyclone.error("sound: " .. Cyclone.nsh.error(args[1], "Sound"))
			return nil
		end
		
		local old_state = sound:isPlaying()
		local new_state
		if args[2] == "play" then new_state = true
		elseif args[2] == "stop" then new_state = false
		else new_state = not old_state
		end
		if (not old_state) and new_state then
			sound:play(1, 1)
			Cyclone.print(string.format("Sound %s started", sound:getName()))
		elseif old_state and (not new_state) then
			sound:stop()
			Cyclone.print(string.format("Sound %s stopped", sound:getName()))
		end
	end
end)