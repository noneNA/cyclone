-- Cyclone - contents/terminal.lua

-- Dependencies:
---- CycloneLib.table

local OUTPUT_SIZE = 500
local PROMPT = "$ "

local ALIAS_BREAK = {
	"",
	"\t",
	"\n",
	" ",
	'"',
	"'",
}
ALIAS_BREAK = CycloneLib.table.swap(ALIAS_BREAK)

local output = {}
local function resetOutput()
	output.line = {}
	output.color = {}
	output.print = true
	output.logging = false
end
resetOutput()
registercallback("onGameEnd", resetOutput)

output.addLine = function(text, color)
	if output.logging then log(text) end
	local color = color or Color.WHITE
	if #output.line >= OUTPUT_SIZE then
		table.remove(output.line, 1)
		table.remove(output.color, 1)
	end
	table.insert(output.line, text)
	table.insert(output.color, color)
end

local commands = {}
local aliases = {}

local function hasAlias(command, alias_name)
	local start, _end = string.find(command, alias_name, 1, true)
	return (start == 1) and ALIAS_BREAK[string.sub(command, _end + 1, _end + 1)]
end

local function resolve_alias(command)
	local command = command
	local alias_count
	repeat
		alias_count = 0
		for name,alias in pairs(aliases) do
			if hasAlias(command, name) then
				alias_count = alias_count + 1
				command = string.gsub(command, name, alias, 1)
				break
			end
		end
	until (alias_count <= 0)
	return command
end

-- Parses received command to argument list
function parseArgs(command)
	local args = {}
	local single, double, cut, escape = false, false
	local argument = ""
	local _i = 0
	for i=1,#command do
		local _char = command:sub(i,i)
		if escape then
			argument = argument .. _char
			escape = false
		elseif _char == "\\" then
			escape = true
		elseif single then
			if _char == "'" then
				single = false
			else argument = argument .. _char end
		elseif _char == "'" then
			cut = true ; single = true
		elseif double then
			if _char == "\"" then
				double = false
			else argument = argument .. _char end
		elseif _char == "\"" then
			cut = true ; double = true
		elseif _char:find("%s") then cut = true
		else argument = argument .. _char end
		if (i == #command) or cut then
			if argument:find("%S") then
				args[_i] = argument
				argument = ""
				_i = _i + 1
			end
			cut = false
		end
	end
	return args
end


--#########--
-- Exports --
--#########--

local Terminal = {}

Terminal.getPrompt = function() return PROMPT end
Terminal.getOutputLines = function() return output.line, output.color end
Terminal.resetOutput = resetOutput

Terminal.print = function(...)
	if not output.print then
		return nil
	end
	local args = {...}
	local length = select("#", ...)
	local color = args[#args]
	if type(color) == "Color" then
		table.remove(args, #args)
		length = length - 1
	else color = Color.WHITE end
	local line = ""
	for i=1,length do
		if #line ~= 0 then line = line .. " " .. tostring(args[i])
		else line = tostring(args[i]) end
	end
	output.addLine(line, color)
end
Terminal.error = function(...) Terminal.print(..., Color.RED) end
Terminal.setLogging = function(value)
	if value then output.logging = true
	else output.logging = false end
end

Terminal.addCommand = function(name, command)
	if commands[name] then
		print("Warning: Cyclone Terminal command '" .. name .. "' has been overridden")
	end
	commands[name] = command
end
Terminal.getCommands = function()
	return CycloneLib.table.shallowcopy(commands)
end

Terminal.addAlias = function(name, alias)
	if hasAlias(alias, name) then
		print(string.format("Warning: Not aliasing %q to %q since it is recursive", name, alias))
		return nil
	end
	if aliases[name] then
		print(string.format("Warning: Cyclone Terminal alias '%s' has been overridden", name))
	end
	aliases[name] = alias
end
Terminal.removeAlias = function(alias_name)
	for name,alias in pairs(aliases) do
		if name == alias_name then
			aliases[name] = nil
			return nil
		end
	end
end
Terminal.getAliases = function()
	return CycloneLib.table.shallowcopy(aliases)
end

Terminal.run = function(command, noalias)
	local command = command
	if not noalias then command = resolve_alias(command) end
	local raw, args = command, parseArgs(command)
	local command_function = commands[args[0]]
	if command_function then command_function(args, raw)
	elseif args[0] ~= nil then Terminal.error(string.format("'%s': Command not found, try the command 'help' or 'list'", args[0])) end
end

Terminal.silentRun = function(command, noalias)
	output.print = false
	Terminal.run(command, noalias)
	output.print = true
end


--#########--
-- Exports --
--#########--

export("Cyclone.Terminal", Terminal)
export("Cyclone.print", Terminal.print)
export("Cyclone.error", Terminal.error)
return Terminal