-- Cyclone - contents/terminal_gui.lua

-- Dependencies:
---- Terminal
---- Cyclone.sc
---- TextInput

local HISTORY_SIZE = 500
local GRAPHICS_COLOR = true

local STRING_WORKAROUND = 2000

local FONT_NAMES = {
	"FONT_DEFAULT",
	"FONT_LARGE",
	"FONT_SMALL",
	"DejaVu",
	"Basis33",
}
local FONTS = {
	graphics.FONT_DEFAULT,
	graphics.FONT_LARGE,
	graphics.FONT_SMALL,
	Cyclone.resources.fonts.dejavu,
	Cyclone.resources.fonts.basis,
}

local input_field  = Cyclone.TextInput.new()

local history = {}
local history_index = 0
local function resetHistory() history = {} ; history_index = 0 end
callback.register("onGameStart", resetHistory)

local visible = false
local function resetVisible() visible = false end
callback.register("onGameStart", resetVisible)

local function openGUI() Cyclone.pause.detach() ; input_field:activate() ; visible = true end
local function closeGUI() Cyclone.pause.attach() ; input_field:deactivate() ; visible = false end

local function graphics_print(text, x, y, font, colored)
	if colored == nil then colored = GRAPHICS_COLOR end
	local name = colored and "printColor" or "print"
	graphics[name](text, x, y, font)
end

registercallback("onStep", function()
	local command = input_field:pop()
	if command then
		if #history >= HISTORY_SIZE then
			table.remove(history, 1)
		end
		table.insert(history, command)
		history_index = 0
		Cyclone.Terminal.print(Cyclone.Terminal.getPrompt() .. command, Color.LIGHT_GREEN)
		Cyclone.Terminal.run(command)
	end
	
	local f12 = input.checkKeyboard("F12") == input.PRESSED
	local shf = input.checkKeyboard("shift") == input.HELD
	if f12 then
		if shf and visible then
			input_field:toggle()
			Cyclone.pause.toggleDetach()
		else
			if visible then closeGUI()
			else openGUI()
			end
		end
	end
	
	if input_field.active then
		if input.checkKeyboard("up") == input.PRESSED then
			history_index = math.min(#history, history_index + 1)
			local command = history[#history + 1 - history_index]
			if command then
				input_field.value = command
				input_field.cursor = #command
			end
		elseif input.checkKeyboard("down") == input.PRESSED then
			history_index = math.max(0, history_index - 1)
			local command = history[#history + 1 - history_index]
			if command then
				input_field.value = command
				input_field.cursor = #command
			end
		end
	end
end)

local surface, surface_x, surface_y
local function initSurface()
	local w,h = graphics.getHUDResolution()
	surface = Surface.new(w*(5/16)*Cyclone.sc, h*(1/4)*Cyclone.sc)
	surface_x = w*(1/16)
	surface_y = h*(1-1/4)
end
local function resizeSurface(w,h)
	local _w,_h = graphics.getHUDResolution()
	if w < 0 then w = _w*(5/16) end
	if h < 0 then h = _h*(1/4) end
	if type(surface) == "Surface" then surface:free() end
	surface = Surface.new(w*Cyclone.sc, h*Cyclone.sc)
end
local function moveSurface(x,y,relative)
	if relative then surface_x = surface_x + x ; surface_y = surface_y + y
	else surface_x = x ; surface_y = y
	end
end
local function refreshSurface()
	if type(surface) == "Surface" then surface:free() end
	initSurface()
end
registercallback("onGameStart", refreshSurface)

-- Inserts "\n" in order to make the line fit a certain width
-- Optionally calculates cursor position
local function wrapLine(line, width, font, cursor)
	--local wrapped_line = ""
	local wrapped = {}
	
	local cursor_index = cursor or 0
	local cursor_x, cursor_y = graphics.textWidth(line:sub(1, cursor_index), font), 0
	local cursor_char = line:sub(cursor_index + 1, cursor_index + 1)
	local cursor_width = graphics.textWidth(cursor_char ~= "" and cursor_char or " ", font)
	local cursor_height = graphics.textHeight(line, font)
	
	local estimate_char_width = graphics.textWidth(" ", font)
	local char_count = math.floor(width/estimate_char_width)
	
	while graphics.textWidth(line, font) > width do
		local wrap, c, n
		-- This part shouldn't loop most of the time
		repeat
			wrap = line:sub(1, char_count + 1)
			n = graphics.textWidth(wrap, font) > width
			wrap = line:sub(1, char_count)
			c = graphics.textWidth(wrap, font) <= width
			if     not c then char_count = char_count - 1
			elseif not n then char_count = char_count + 1
			end
		until (c and n)
		if cursor_index > char_count then
			cursor_y = cursor_y + graphics.textHeight(wrap, font)
			cursor_x = cursor_x - graphics.textWidth(wrap, font)
			cursor_index = cursor_index - char_count
			cursor_height = graphics.textHeight(line:sub(char_count+1, #line), font)
		else
			cursor_height = graphics.textHeight(wrap, font)
		end
		--wrapped_line = wrapped_line .. wrap .. "\n"
		table.insert(wrapped, wrap)
		table.insert(wrapped, "\n")
		line = line:sub(char_count+1, #line)
	end
	--wrapped_line = wrapped_line .. line
	table.insert(wrapped, line)
	
	--[[
	if height then
		local partial_line = ""
		local i = #wrapped_line
		for i=#wrapped_line,1,-1 do
			local new_char = wrapped_line:sub(i, i)
			if graphics.textHeight(partial_line .. new_char, font) > height then
				break
			else
				partial_line = new_char .. partial_line
			end
		end
		wrapped_line = partial_line
	end
	--]]
	
	--return wrapped_line, cursor_x, cursor_y, cursor_width, cursor_height
	return table.concat(wrapped), cursor_x, cursor_y, cursor_width, cursor_height
end

-- This should be on a different mod
local sc = Cyclone.sc
callback.register("preStep", function()
	if sc ~= Cyclone.sc then
		refreshSurface()
		sc = Cyclone.sc
	end
end)

-- I think this is something about lua but don't know how to fix it
-- 1 works normally but 2 and 3 slow the game to a crawl at wrapLine (even though the first doesn't)
-- The line above the wrapLine call is a workaround for this
-- Change STRING_WORKAROUND to something large and uncomment these to test it
--[[
local sf = Cyclone.nsh.suggestions("f", "Sprite")
Cyclone.print(sf) -- 1
callback.register("postLoad", function()
	sf = Cyclone.nsh.suggestions("f", "Sprite")
	--Cyclone.print(sf) -- 2
end)
Cyclone.Terminal.addCommand("sf", function()
	--sf = Cyclone.nsh.suggestions("f", "Sprite")
	--Cyclone.print(sf) -- 3
end)
--]]

registercallback("onHUDDraw", function()
	if visible then
		local font = FONTS[4]
		
		if not surface:isValid() then
			refreshSurface()
		end
		graphics.setTarget(surface)
		
		graphics.color(Color.BLACK) ; graphics.alpha(0.6)
		graphics.rectangle(0, 0, surface.width, surface.height)

		graphics.color(Color.WHITE) ; graphics.alpha(1)
		
		local wrapped_line, cursor_x, cursor_y
		local y = surface.height
		
		local prompt = Cyclone.Terminal.getPrompt()
		wrapped_line, cursor_x, cursor_y, cursor_width, cursor_height = wrapLine(prompt .. input_field.value, surface.width, font, input_field.cursor + #prompt)
		y = y - graphics.textHeight(wrapped_line, font)
		
		graphics.color(Color.DARK_GREEN) ; graphics.alpha(0.8)
		graphics.rectangle(cursor_x, y + cursor_y, cursor_x + cursor_width * (input_field.active and 1 or 0), y + cursor_y + cursor_height - 1)
		
		graphics.color(Color.GREEN) ; graphics.alpha(1)
		graphics_print(wrapped_line, 0, y, font, false)
		
		local outputLines, outputColor = Cyclone.Terminal.getOutputLines()
		for i=#outputLines,1,-1 do
			local line = outputLines[i]:gsub("\t", "  ")
			local length = #line
			if length > STRING_WORKAROUND then
				line = "The beginning of this text has been cut due to being too long\n" .. line:sub(length - STRING_WORKAROUND, length)
			end
			wrapped_line = wrapLine(line, surface.width, font)
			y = y - graphics.textHeight(wrapped_line, font)
			graphics.color(outputColor[i])
			graphics_print(wrapped_line, 0, y, font)
			if y <= 0 then break end
		end

		graphics.resetTarget()
		
		local w,h = graphics.getHUDResolution()
		graphics.drawImage{
			image = surface,
			x = surface_x,
			y = surface_y,
			scale = (1/Cyclone.sc),
		}
	end
end)


--#########--
-- Exports --
--#########--

local Terminal_GUI = {}

Terminal_GUI.open  = openGUI
Terminal_GUI.close = closeGUI
Terminal_GUI.move = moveSurface
Terminal_GUI.resize = resizeSurface
Terminal_GUI.reset = refreshSurface

export("Cyclone.Terminal_GUI", Terminal_GUI)